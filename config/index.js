const loadConfig = require('../src/lib/loadConfig');
const configSchema = require('./config.schema');

module.exports = loadConfig(__dirname, configSchema);