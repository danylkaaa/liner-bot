const config = require('../config');
const runMigrations = require('../src/lib/runMigrations');
const path = require('path');
const knex = require('../src/connections').knex(config.postgres);
const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');

const optionDefinitions = [
  {
    name: 'help',
    alias: 'h',
    type: Boolean,
    description: 'Display help information'
  },
  {
    name: 'last',
    alias: 'l',
    type: Number,
    description: 'Last used migration time'
  },
  {
    name: 'dir',
    alias: 'd',
    type: String,
    description: 'Directory with migrations'
  },
  {
    name: 'src',
    alias: 's',
    type: String,
    multiple: true,
    defaultOption: true,
    description: 'List of migrations, should be applied'
  }
];
const sections = [
  {
    header: 'SQL migrations',
    content: 'Run SQL migrations, stored in specified folder'
  },
  {
    header: 'Synopsis',
    content: '$ node migrations.js <options> '
  },
  {
    header: 'Options',
    optionList: optionDefinitions,
  }
];

const options = commandLineArgs(optionDefinitions);
if (options.dir === undefined) {
  console.error('Parameter --dir is required');
  process.exit(1);
}
if (options.help) {
  console.log(commandLineUsage(sections));
} else {
  runMigrations(knex, path.join(process.cwd(), options.dir), options).then(() => {
    console.log('done');
    knex.destroy();
  });
}
