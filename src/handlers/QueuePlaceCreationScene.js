const constants = require('../constants');
const WizardScene = require('telegraf/scenes/wizard');
const extra = require('telegraf/extra');
const Composer = require('telegraf/composer');
const { leave, enter } = require('telegraf/stage');
const moment = require('moment');
const calendar = require('../calendar');

const YES = { text: 'да', id: 'yes' };
const NO = { text: 'нет', id: 'no' };

const isCorrectJob = new Composer();
isCorrectJob.action(YES.id, (ctx) => ctx.wizard.next());
isCorrectJob.action(NO.id, enter(constants.scenes.queue));

const isCorrectTime = new Composer();
isCorrectTime.action(YES.id, (ctx) => ctx.wizard.next());
isCorrectTime.action(NO.id, (ctx) => {
  ctx.wizard.cursor -= 3;
  return ctx.wizard.next();
});

const isCorrectTotal = new Composer();
isCorrectTotal.action(YES.id, (ctx) => ctx.wizard.next());
isCorrectTotal.action(NO.id, enter(constants.scenes.queue));

const doesUserWantSyncGC = new Composer();
doesUserWantSyncGC.action(YES.id, (ctx) => ctx.wizard.next());
doesUserWantSyncGC.action(NO.id, enter(constants.scenes.queue));


function askIdOfJob(ctx) {
  ctx.reply('Шаг 1\nВведите `id` специалиста', extra.markdown());
  return ctx.wizard.next();
}

async function askIsCorrectJob(ctx) {
  const job = await ctx.Jobs.query().findById(ctx.message.text);
  if (!job) {
    ctx.reply('К сожалению специалист с таким id не найден');
    return ctx.scene.enter(constants.scenes.queue);
  }
  ctx.session.jobUid = job.uid;
  ctx.reply(`Вы имели ввиду этого специалиста?\n${job.toMD()}`,
    extra.markdown()
      .markup(m => m
        .inlineKeyboard(
          [YES, NO].map(t => m.callbackButton(t.text, t.id)),
          { columns: 2 }
        )));
  return ctx.wizard.next();
}

function askTime(ctx) {
  ctx.reply('Введите дату посещения в формате `МЕСЯЦ`-`ДЕНЬ`-`ГОД` `ЧАСЫ:МИНУТЫ`', extra.markdown());
  return ctx.wizard.next();
}

function askIsCorrectTime(ctx) {
  if (!moment(ctx.message.text).isValid()) {
    ctx.reply('Вы ошиблись в формате, повторите попытку');
    ctx.wizard.cursor -= 2;
    return ctx.wizard.next();
  }
  const time = moment(ctx.message.text).locale('ru');
  ctx.reply(`Вы уверены, что хотите встать встать в очердь в это время?\n${time.format('llll')}`,
    extra.markdown()
      .markup(m => m
        .inlineKeyboard(
          [YES, NO].map(t => m.callbackButton(t.text, t.id)),
          { columns: 2 }
        )));
  ctx.session.time = time;
  return ctx.wizard.next();
}

async function askTotal(ctx) {
  const job = await ctx.Jobs.query().findById(ctx.session.jobUid);
  const time = moment(ctx.session.time).locale('ru');
  ctx.reply(`Вы уверены, что хотите встать в очередь к ${job.toMD()}\nВ *${time.format('llll')}*`, extra.markdown()
    .markup(m => m
      .inlineKeyboard(
        [YES, NO].map(t => m.callbackButton(t.text, t.id)),
        { columns: 2 }
      )));
  return ctx.wizard.next();
}

async function createQueuePlace(ctx) {
  const time = moment(ctx.session.time);
  const job = await ctx.Jobs.query().findById(ctx.session.jobUid);
  const { uid } = await ctx.QueuePlaces.query().insert({
    userUid: ctx.$user.uid,
    jobUid: ctx.session.jobUid,
    time
  });
  const qp = await ctx.QueuePlaces.query().findById(uid).eager('[job.user, user]');
  qp.addEventToJob().then(() => {
    console.log('Event for job created');
  });
  ctx.reply(
    'Вы заняли место в очереди:\n'
    + `Когда: ${moment(qp.time).locale('ru').format('llll')}\n`
    + `К кому: ${job.toMD()}`, extra.markdown()
  );
  if (ctx.$user.gcAccessToken) {
    ctx.session.qpUid = qp.uid;
    ctx.reply('Вы хотите создать напоминание в Гугл календаре?', extra
      .markup(m => m
        .inlineKeyboard(
          [YES, NO].map(t => m.callbackButton(t.text, t.id)),
          { columns: 2 }
        )));
    return ctx.wizard.next();
  }
  return ctx.scene.enter(constants.scenes.queue);
}

async function syncGC(ctx) {
  const qs = await ctx.QueuePlaces.query().findById(ctx.session.qpUid).eager('[job.user, user]');
  const res = await qs.addEventToUser();
  if (!res) {
    ctx.reply('Кажется у вас не подключен Гугл календарь, сначала настройте его');
  } else {
    ctx.reply(`Готово! Можете просмотреть событие по [ссылке](${res.data.htmlLink}`, extra.markdown());
  }
  return ctx.scene.enter(constants.scenes.queue);
}

const steps = [
  askIdOfJob,
  askIsCorrectJob,
  isCorrectJob,
  askTime,
  askIsCorrectTime,
  isCorrectTime,
  askTotal,
  isCorrectTotal,
  createQueuePlace,
  doesUserWantSyncGC,
  syncGC
];


class QueuePlaceCreationScene extends WizardScene {
  static get name() {
    return constants.scenes.queuePlaceCreation;
  }

  constructor() {
    super(QueuePlaceCreationScene.name, ...steps);
    this.hears('exit', leave());
  }
}

module.exports = QueuePlaceCreationScene;
