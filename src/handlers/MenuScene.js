const Scene = require('telegraf/scenes/base');
const Markup = require('telegraf/markup');
const constants = require('../constants');
const { compose } = require('telegraf');
const { enter } = require('telegraf/stage');


function showMenu(ctx) {
  ctx.reply('Выберите категорию', Markup
    .keyboard([
      constants.commands.enterJobsScene,
      constants.commands.enterQueueScene,
      constants.commands.addGCKey,
      constants.commands.viewPolicy
    ]).oneTime().resize().extra());
}

async function register(ctx, next) {
  ctx.$user = await ctx.Users.findOrCreateByChat(await ctx.getChat());
  await next();
}

class MenuScene extends Scene {
  static get name() {
    return constants.scenes.menu;
  }

  constructor() {
    super(MenuScene.name);
    this.enter(compose([register, showMenu]));
    this.hears(constants.commands.enterJobsScene, enter(constants.scenes.jobs));
    this.hears(constants.commands.viewPolicy, enter(constants.scenes.policy));
    this.hears(constants.commands.enterQueueScene, enter(constants.scenes.queue));
    this.hears(constants.commands.addGCKey, enter(constants.scenes.gcIntegration));
    // this.on('message', compose([register, showMenu]));
  }
}

module.exports = MenuScene;
