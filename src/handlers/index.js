const Stage = require('telegraf/stage');
const PolicyScene = require('./PolicyScene');
const MenuScene = require('./MenuScene');
const JobsScene = require('./JobsScene');
const JobCreationScene = require('./JobCreationScene');
const JobDeletionScene = require('./JobDeletionScene');
const QueuePlaceCreationScene = require('./QueuePlaceCreationScene');
const GCIntegrationScene = require('./GCIntegrationScene');
const QueueScene = require('./QueueScene');
const constants = require('../constants');

const { enter } = Stage;
const policyScene = new PolicyScene(enter(constants.scenes.menu), cancel);
const menuScene = new MenuScene();
const jobsScene = new JobsScene();
const jobCreationScene = new JobCreationScene();
const jobDeletionScene = new JobDeletionScene();
const queuePlaceCreationScene = new QueuePlaceCreationScene();
const queueScene = new QueueScene();
const gCIntegrationScene = new GCIntegrationScene();

// Create scene manager
const stage = new Stage([
  policyScene,
  jobsScene,
  gCIntegrationScene,
  jobDeletionScene,
  menuScene,
  jobCreationScene,
  queuePlaceCreationScene,
  queueScene
], { default: constants.scenes.menu });


async function onStart(ctx) {
  const chat = await ctx.getChat();
  ctx.reply(`Привет, ${chat.first_name}`);
  ctx.scene.enter(constants.scenes.menu);
}


function cancel(ctx) {
  ctx.reply('Есди вы когда-нибудь передумаете, нажмите /start');
  ctx.scene.leave();
}

exports.init = bot => {
  bot.use(stage.middleware());
  bot.start(onStart);
};
