const constants = require('../constants');
const WizardScene = require('telegraf/scenes/wizard');
const extra = require('telegraf/extra');


const steps = [
  ctx => {
    ctx.reply([
      'Шаг 1',
      'Добавьте введите id удаляемой работы'
    ].join('\n'));
    return ctx.wizard.next();
  },
  async ctx => {
    const job = await ctx.$user.$relatedQuery('jobs').where({ uid: ctx.message.text }).first();
    if (!job) {
      ctx.reply('Кажется, у вас нет должности с таким id');
      return ctx.scene.leave();
    }
    await job.$query().del();
    ctx.reply(`ваша должность ${job.toMD()} успешно удалена`, extra.markdown());
    return ctx.scene.leave();
  },

];


class JobDeletionScene extends WizardScene {
  static get name() {
    return constants.scenes.jobDeletion;
  }

  constructor() {
    super(JobDeletionScene.name, ...steps);
  }
}

module.exports = JobDeletionScene;
