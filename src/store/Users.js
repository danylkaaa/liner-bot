const BaseModel = require('./BaseModel');
const { Model } = require('objection');
const uuid = require('uuid/v4');

class Users extends BaseModel {
  static get idColumn() {
    return 'uid';
  }

  static get tableName() {
    return 'users';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        uid: { type: 'string' },
        telegramId: { type: 'string' },
        username: { type: 'string', minLength: 1, maxLength: 255 },
        firstName: { type: 'string', maxLength: 255 },
        lastName: { type: 'string', maxLength: 255 },
        updateAt: { type: 'date' },
        createdAt: { type: 'date' },
        gcAccessToken: { type: 'string' },
        gcRefreshToken: { type: 'string' }
      }
    };
  }

  static get relationMappings() {
    return {
      queuePlaces: {
        relation: Model.HasManyRelation,
        modelClass: `${__dirname}/${'QueuePlaces'}`,
        join: {
          from: 'users.uid',
          to: 'queue_places.user_uid'
        }
      },
      jobs: {
        relation: Model.HasManyRelation,
        modelClass: `${__dirname}/${'Jobs'}`,
        join: {
          from: 'users.uid',
          to: 'jobs.user_uid'
        }
      }
    };
  }

  static async create(data, opts = {}) {
    const user = await Users.query(opts.trx).insertGraph(data);
    return Users.query(opts.trx).findById(user[Users.idColumn]);
  }

  static async findOrCreateByChat(chat, opts = {}) {
    const user = await Users.query(opts.trx).where({ telegramId: chat.id }).first();
    if (user) return user;
    return this.create({
      telegramId: String(chat.id),
      firstName: chat.first_name,
      secondName: chat.second_name,
      username: chat.username
    }, opts);
  }

  static async findByChat(chat, opts = {}) {
    const user = await Users.query(opts.trx).where({ telegramId: chat.id }).first();
    if (user) return user;
    return null;
  }

  async $beforeInsert(queryContext) {
    this[Users.idColumn] = uuid();
    await super.$beforeInsert(queryContext);
  }

  gcTokens() {
    return {
      access_token: this.gcAccessToken,
      refresh_token: this.gcRefreshToken
    };
  }
}

module.exports = Users;
