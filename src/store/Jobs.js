const BaseModel = require('./BaseModel');
const { Model } = require('objection');
const uuid = require('uuid/v4');

class Jobs extends BaseModel {
  static get tableName() {
    return 'jobs';
  }

  static get idColumn() {
    return 'uid';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [
        'userUid',
        'title'
      ],
      properties: {
        uid: { type: 'string' },
        userUid: { type: 'string' },
        title: { type: 'string', minLength: 1, maxLength: 255 },
      }
    };
  }

  static get relationMappings() {
    return {
      queuePlaces: {
        relation: Model.HasManyRelation,
        modelClass: `${__dirname}/${'QueuePlaces'}`,
        join: {
          from: 'jobs.uid',
          to: 'queue_places.jobUid'
        }
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: `${__dirname}/${'Users'}`,
        join: {
          from: 'jobs.userUid',
          to: 'users.uid'
        }
      }
    };
  }

  toMD() {
    return `${this.title} \`${this.uid}\``;
  }

  async $beforeInsert(queryContext) {
    this[Jobs.idColumn] = uuid();
    await super.$beforeInsert(queryContext);
  }
}

module.exports = Jobs;
