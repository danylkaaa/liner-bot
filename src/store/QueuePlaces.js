const BaseModel = require('./BaseModel');
const { Model } = require('objection');
const uuid = require('uuid/v4');
const moment = require('moment');
const calendar = require('../calendar');

class QueuePlaces extends BaseModel {
  static get idColumn() {
    return 'uid';
  }

  static get tableName() {
    return 'queue_places';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        uid: { type: 'string' },
        userUid: { type: 'string' },
        jobUid: { type: 'string' },
        time: { type: 'date' },
        createdAt: { type: 'date' },
        updatedAt: { type: 'data' }
      }
    };
  }

  static get relationMappings() {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: `${__dirname}/${'Users'}`,
        join: {
          from: 'queue_places.userUid',
          to: 'users.uid'
        }
      },
      job: {
        relation: Model.BelongsToOneRelation,
        modelClass: `${__dirname}/${'Jobs'}`,
        join: {
          from: 'queue_places.jobUid',
          to: 'jobs.uid'
        }
      }
    };
  }

  toMD() {
    return `В *${moment(this.time).locale('ru').format('llll')}* к \`${this.jobUid}\``;
  }

  async $beforeInsert(queryContext) {
    this[QueuePlaces.idColumn] = uuid();
    await super.$beforeInsert(queryContext);
  }


  async addEventToJob() {
    if (!this.job.user.gcAccessToken || !this.job.user.gcRefreshToken) {
      return null;
    }
    const event = {
      summary: `К вам посетитель ${this.user.firstName} ${this.user.secondName || ''}`,
      description: `К вам в очередь встал новый посетитель https://t.me/${this.user.username}`,
      start: {
        dateTime: this.time,
        timeZone: 'Europe/Kiev',
      },
      end: {
        timeZone: 'Europe/Kiev',
        dateTime: moment(this.time).add(30, 'minutes')
      },
      recurrence: ['RRULE:FREQ=DAILY;COUNT=1'],
      reminders: {
        useDefault: false,
        overrides: [
          { method: 'email', minutes: 24 * 60 },
          { method: 'popup', minutes: 5 },
        ],
      },
    };
    return calendar.addEvent(event, calendar.getOAuth2Client(this.job.user.gcTokens()));
  }

  async addEventToUser() {
    if (!this.user.gcAccessToken || !this.user.gcRefreshToken) {
      return null;
    }
    const event = {
      summary: `Посещение ${this.job.user.firstName}|${this.job.title}`,
      description: `Вы встали в очередь на посещение этого специалиста https://t.me/${this.job.user.username}`,
      start: {
        dateTime: this.time,
        timeZone: 'Europe/Kiev',
      },
      end: {
        timeZone: 'Europe/Kiev',
        dateTime: moment(this.time).add(30, 'minutes')
      },
      recurrence: ['RRULE:FREQ=DAILY;COUNT=1'],
      reminders: {
        useDefault: false,
        overrides: [
          { method: 'email', minutes: 24 * 60 },
          { method: 'popup', minutes: 60 },
          { method: 'popup', minutes: 120 },
        ],
      },
    };
    return calendar.addEvent(event, calendar.getOAuth2Client(this.user.gcTokens()));
  }
}

module.exports = QueuePlaces;
