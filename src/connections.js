const { knexSnakeCaseMappers } = require('objection');
const knex = require('knex');
const RedisSession = require('telegraf-session-redis');

module.exports.knex = config => knex({
  client: config.dialect,
  connection: {
    host: config.host,
    port: config.port,
    user: config.user,
    password: config.password,
    database: config.database
  },
  acquireConnectionTimeout: 10000,
  ...config,
  pool: Object.assign({
    min: 1,
    max: 50,
  }, config.pool),
  ...knexSnakeCaseMappers()
});

module.exports.redisSession = config => new RedisSession({
  store: config
});
