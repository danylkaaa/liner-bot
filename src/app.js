const config = require('../config');
const Logger = require('./lib/Logger');
const handlers = require('./handlers');
const Users = require('./store/Users');
const QueuePlaces = require('./store/QueuePlaces');
const Jobs = require('./store/Jobs');
const Telegraf = require('telegraf');
const connections = require('./connections');
const auth = require('./middlewares/auth');
const { Model } = require('objection');

const knex = connections.knex(config.postgres);

new Logger(config.logger).patchLoggers();
Model.knex(knex);

const bot = new Telegraf(process.env.BOT_TOKEN);
bot.context.Users = Users;
bot.context.Jobs = Jobs;
bot.context.QueuePlaces = QueuePlaces;

bot.use(connections.redisSession(config.redis).middleware());
bot.use(auth);
bot.use(async (ctx, next) => {
  const start = new Date();
  try {
    await next(ctx);
  } catch (e) {
    console.error('%O', e);
    ctx.reply('Server error!');
  }
  const ms = new Date() - start;
  console.log('Response time %sms', ms);
});

handlers.init(bot);
bot.startPolling();

console.info('server up');
